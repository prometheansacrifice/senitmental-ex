require 'sentimental'

analyzer = Sentimental.new

# Load the default sentiment dictionaries
analyzer.load_defaults

# Set a global threshold
analyzer.threshold = 0.25

# Use your analyzer
inp = $stdin.read
print analyzer.sentiment inp
print "\n"
